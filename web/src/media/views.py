from django.views import View
from django.http import HttpResponse
from django.utils.http import urlquote

class Media(View):
    def get(self, request, filePath):
        filePath = urlquote(filePath)
        response = HttpResponse()
        url = '/protectedMedia/{}'.format(filePath)
        response['X-Accel-Redirect'] = url
        response['Content-Type'] = ''
        return response
