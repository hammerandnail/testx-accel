#!/bin/sh

while ! nc -z $PGHOST $PGPORT;
do
    echo "wait for database";
    sleep 1;
done;

python manage.py collectstatic --noinput --clear
python manage.py makemigrations --noinput
python manage.py migrate

if [ -n "$SUPERUSER" ] && [ -n "$SUPERUSERPASSWORD" ]
then
#     echo "
# from django.contrib.auth import get_user_model;
# User = get_user_model();
# if not User.objects.filter(username='$SUPERUSER').exists():
#     User.objects.create_superuser('$SUPERUSER', '', '$SUPERUSERPASSWORD')
#     " | python manage.py shell

    echo "
from django.contrib.auth.models import User;
if not User.objects.filter(username='$SUPERUSER').exists():
    User.objects.create_superuser('$SUPERUSER', '', '$SUPERUSERPASSWORD')
    " | python manage.py shell
fi

exec "$@"

# gunicorn -b 0.0.0.0:8000 src.wsgi
python manage.py runserver 0.0.0.0:8000
